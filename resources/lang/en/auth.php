<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'session' => 'Sign in to start your session',
    'username' => 'User Name',
    'password' => 'Password',
    'sign_in' => 'Sign In',
    'forgot' => 'Forgot your password ',
    'forgot_des' => 'Please enter your email account. System will send a email with active link to reset your password. ',
    'email' => 'Email Address ',
    'send' => 'Send Reset ',
    'login' => 'Login ',
    'register' => 'Register',
];
