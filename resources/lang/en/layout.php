<?php
return [
    //---- Dashboard
    'my_reports' => 'My Reports',
    'total' => 'Total',
    'total_users'=> 'Total users',
    'total_comments' => 'Total comments',
    'this_month' => 'This month',
    'reports' => 'Reports',
    'view_more' => 'View more',
    'chat_box' => 'Chat box',
    'chat_box_title' => 'Create a new chat with others users',
    'reports_today' => 'Reports today',
    'reports_today_title' => 'List of users reports today...',
    'table'=>[
        'name' => 'Name',
        'title' => 'Title',
        'time' => 'Time'
    ],

    //---- Sidebar
    'user_profile' => 'User profile',
    'calendar_report' => 'Calendar Report',
    'list_user_report' => 'List User Report',
    'users' => 'Users',
    'roles' => 'Roles',
    'groups' => 'Groups',
    'activity_logs' => 'Activity logs',
    'dashboard'=>'Dashboard',
    'logout'=>'Logout',
    'notification'=>'Empty Notification',
    'reminded'=>'mentioned you in a comment',
    'commented'=>'commented reports of you',
    'setting'=>'Settings',
    'projects'=>'Project',
    'tasks'=>'Task',
    'agile'=>'Agile',

];
