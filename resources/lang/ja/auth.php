<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'session' => 'ログインしてセッションを開始してください',
    'username' => 'ユーザー名',
    'password' => 'パスワード',
    'sign_in' => 'ログイン',
    'forgot' => 'パスワードをお忘れですか',
    'forgot_des' => 'メールアカウントを入力してください。システムは、パスワードをリセットするためのアクティブなリンクを含むメールを送信します。 ',
    'email' => '電子メールアドレス ',
    'send' => 'リセットを送信 ',
    'login' => 'ログインする ',
    'register' => '登録',
];
