<?php
return [
    //----- report
    'list'             => 'レポート一覧',
    'add'              => 'レポートを追加',
    'edit'             => 'レポートを更新',
    'name'             => '名前',
    'title'            => '題名',
    'content'          => 'コンテンツ',
    'message'          => '連絡事項',
    'detail'           => '細部',
    'created_at'       => 'で作成',
    'created_by'       => 'によって作成された',
    'date'             => '日付',
    'show'             => '公演',
    'entries'          => 'エントリー',
    'search'           => '探す',
    'comment'             => 'コメント',
    'nodata'          => '該当する記録が見つかりません',
    'this-week'           => '今週',
    'last-week'             => '先週',
    'this-month'          => '今月',
    'last-month'           => '先月',
    'this-year'             => '今年',
    'input-message'          => 'あなたの入力メッセージ',
    'comment-report'           => 'コメントユーザーレポート',
    'showing'           => '表示中',
    'of'           => 'の',
    'pre'           => '前',
    'next'           => '次',
    'select-date' =>'日時を選択',
    'edit-report'           => 'レポートイベントの編集',
    'color'           => '色',
    'add-report'           => 'レポートイベントを追加',
    'start'           => '開始',
    'save'=>'保存する',
    'tasks'=>'課題',
    'work_content'=>'作業内容',
    'task_no'           => 'タスクが必要です',
    'content_no'           => '仕事内容が必要です',
    'day'=>'月日',
    'export'=>'書き出す',
    'export_title'=>'タイトルExcelファイルをエクスポート',
    'export_time'=>'エクスポートする時間を選択してください',
];
