<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="user-id" content="{{ Auth::user()->id }}">
    <meta name="user-name" content="{{ Auth::user()->name }}">
    <meta name="api-base-url" content="{{ url('/') }}"/>
    <meta name="url_file" content="{{ url('/') }}"/>
    <meta name="locale" content="{{ App::getLocale() }}"/>
    {{-- <title>{{ setting('admin_title',config('app.name')) }}</title> --}}
    {{-- <link rel="shortcut icon" href="{{asset('/storage/avatar/'.setting('admin_favicon','favicon.png'))}}" sizes="32x32"> --}}
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/material-font.css')}}">

    <link href="{{asset('css/main.css')}}" rel="stylesheet"/>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('lib/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
    <!-- Jquery UI -->
    <link rel="stylesheet" href="{{ asset('lib/jquery/jquery-ui/jquery-ui.min.css') }}">
    <!-- DHTMLX Gantt chart  -->
    <link rel="stylesheet" href="{{ asset('lib/dhtmlx/dhtmlx.css') }}">
    <link href="{{asset('css/layout.css')}}" rel="stylesheet"/>
</head>
<body>
<div class="wrapper ">
    @include('layouts.sidebar')
    <main class="main-panel" id="app">
        @include('layouts.navbar')
        @include('blocks.message')
        @yield('content')
    </main>
    {{------------------------ Image modal box ---------------------- --}}
    <div id="imageModalBox" class="imageModal">
        <span class="imageModal-close">&times;</span>
        <img class="imageModal-content" id="imageModalBoxSrc">
    </div>
</div>
<!-- jQuery -->
<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap  -->
<script src="{{ asset('lib/material/js/core/popper.min.js') }}"></script>
<script src="{{ asset('lib/material/js/core/bootstrap-material-design.min.js') }}"></script>

@stack('page-scripts')
<script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('lib/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lib/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{asset('lib/material/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!-- Select 2 -->
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<!-- DHTML Gantt chart JS -->
<script src="{{ asset('lib/dhtmlx/dhtmlx.js') }}"></script>

<script src="http://calendar.test/js/main.js"></script>
<script src="{{asset('js/autosize.js')}}"></script>
<script src="{{ asset('js/chat.js') }}"></script>
{{--<script src="{{ asset('js/dashboard.js') }}"></script>--}}
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
        let url = window.location;
        let element = $('.nav .nav-item a').filter(function () {
            return this.href == url || window.location.pathname == 0;
        });
        $(element).parentsUntil('ul.sidebar-menu', 'li').addClass('active');
        $('#notifications').click(function () {
            $('#list-notifications').toggle()
        });
        $('#languageDropdown').click(function () {
            $('#list-language').toggle()
        })
    });
</script>
@stack('script')

</body>
</html>
