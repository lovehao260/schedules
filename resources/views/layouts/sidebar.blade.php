<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('lib/material/img/sidebar-1.jpg')}}">
    <div class="logo">
        <a href="{{url('/')}}" class="simple-text logo-normal">
            <img src="{{asset('/storage/avatar/logo.png')}}">
        </a></div>
    <div class="sidebar-wrapper ps-container ps-theme-default" data-ps-id="842f1926-37f2-1d8e-6e1b-7992efd763ba">
        <ul class="nav">
            <li class="nav-item  ">
                <a class="nav-link" href="{{url('dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <p>{{__("layout.dashboard")}}</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('profile')}}">
                    <i class="fa fa-user-circle"></i>
                    <p>{{__("layout.user_profile")}}</p>
                </a>
            </li>
             <li class="nav-item ">
                <a class="nav-link" href="{{url('agile')}}">
                    <i class="material-icons">content_paste</i>
                    <p>{{__("layout.agile")}}</p>
                </a>
            </li>

             <li class="nav-item ">
                <a class="nav-link" href="{{url('task')}}">
                    <i class="fa fa-list"></i>
                    <p>{{__("layout.tasks")}}</p>
                </a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{ route('user.index') }}">
                    <i class="material-icons">person</i>
                    <p>{{__("layout.users")}}</p>
                </a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{ route('role.index') }}">
                    <i class="material-icons">device_hub</i>
                    <p>{{__("layout.roles")}}</p>
                </a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{ route('group.index') }}">
                    <i class="material-icons">group</i>
                    <p>{{__("layout.groups")}}</p>
                </a>
            </li>

            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ route('front-end.activity.index') }}">
                    <i class="material-icons">flip_to_front</i>
                    <p>{{__("layout.activity_logs")}}</p>
                </a>
            </li> --}}

            {{-- <li class="nav-item ">
                <a class="nav-link" href="{{url('/settings/email')}}">
                    <i class="material-icons">settings</i>
                    <p>{{__("layout.setting")}}</p>
                </a>
            </li> --}}

            <li class="nav-item ">
                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">

                    <i class="material-icons">logout</i>
                    <p> {{ __('layout.logout') }}</p>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </a>
            </li>
        </ul>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </div>
</div>
