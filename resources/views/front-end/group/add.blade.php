@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="content">
        <div class="container-fluid">
            <form role="form" method="POST" action="{{ route('group.post_add') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{__('group.create_group')}}</h4>
                            <p class="card-category">{{__('group.create_new_a_group')}}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('name') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('group.name')}}</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                            @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group">
                                            <label class="bmd-label-floating">{{__('group.description')}}</label>
                                            <textarea class="form-control" rows="2" name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group">
                                            <label>{{__('group.select_user')}}:</label>
                                            <select id ="select_user" class="select-user-box" data-placeholder="{{__('group.select_user')}}" style="width: 50%">
                                                <option value="" selected disabled hidden>{{__('group.select_user')}}</option>
                                                @foreach($users as $user)
                                                    <option value="{{ $user->id }}">
                                                        {{ $user->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <input name="user_ids" value="" hidden/>
                                            <a href="" id="adduser">ADD</a>
                                            <ul class="list_user"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">{{__('action.save')}}</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    //Initialize Select2 Elements
    $('.select-user-box').select2({
        theme: "classic"
    });

    //list user_id selected
    let arr_ids = [];
    var user_ids_element = $("input[name='user_ids']");

    $("#adduser").click(function(e){
        e.preventDefault();
        let name = "";
        let id = $("#select_user").val();
        if(arr_ids.indexOf(id) < 0){
            arr_ids.push(id);
            user_ids_element.val(arr_ids);
            name = $("#select_user option:selected" ).text();
            $('.list_user').append(`<li id="item_${id}"><a onClick="removeItem(${id})">x</a>${name}</li>`);
        }
        console.log(arr_ids);
    });

    function removeItem(id){
        let pos = arr_ids.indexOf(id + "");
        if(pos > -1){
            $("#item_" + id).remove();
            arr_ids.splice(pos, 1);
            $user_ids_element.val(arr_ids);
        }
        console.log(arr_ids);
    }

</script>

@endpush
