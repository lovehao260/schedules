@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="content">
        <div class="container-fluid">
            <form role="form" method="POST" action="{{ route('user.post_add') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{__('user.create_user')}}</h4>
                            <p class="card-category">{{__('user.create_user_title')}}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('name') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.name')}}</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                            @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('username') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.username')}}</label>
                                            <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                                            @error('username')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('email') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.email')}}</label>
                                            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                            @error('email')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('phone') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.phone')}}</label>
                                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                            @error('phone')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('password') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.password')}}</label>
                                            <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                                            @error('password')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('password_confirm') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('user.password_confirm')}}</label>
                                            <input type="password" class="form-control" name="password_confirm" value="{{ old('password_confirm') }}">
                                            @error('password_confirm')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('role_id') has-danger @enderror ">
                                            <label for="inputState">{{__('user.select_role')}}: </label>
                                            <select id="inputState" name="role_id" class="form-control" style="width:50%">
                                                <option value="">--- {{__('user.select_role')}} ---</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" {{ Request::input('role_id') == $role->id? 'selected': ''}}>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">{{__('action.save')}}</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script>
$(document).ready(function(){
    $('#inputState').select2({
        theme: "classic"
    });
});
</script>
@endpush
