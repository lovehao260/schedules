<?php

namespace App\Providers;

use App\ServiceImpl\UserServiceImpl;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class DIServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserService::class, function () {
            return new UserServiceImpl();
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Which IoC bindings the provider provides.
     *
     * @return array
     */
    public function provides()
    {
        //
    }
}
