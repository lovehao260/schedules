<?php


use App\Facades\SettingFacade;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Generate a string with length $byte*2
*/
function random_string($bytes = 4){
    $randomBytes = random_bytes($bytes);
    // encode to get a string
    $randomString = bin2hex($randomBytes);
    return $randomString;
}


if (!function_exists('number_order')) {
    /**
     * @param $stt
     * @return mixed
     * @author Minh Hao
     */
    function number_order($stt)
    {
        return  $stt++;
    }
}
if (!function_exists('date_from_database')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     * @author Minh Hao
     */
    function date_from_database($time, $format = 'Y-m-d')
    {
        if (empty($time)) {
            return $time;
        }
        return format_time(Carbon::parse($time), $format);
    }
}
if (!function_exists('format_time')) {
    /**
     * @param Carbon $timestamp
     * @param $format
     * @return mixed
     * @author Minh Hao
     */
    function format_time(Carbon $timestamp, $format = 'j M Y H:i')
    {
        $first = Carbon::create(0000, 0, 0, 00, 00, 00);
        if ($timestamp->lte($first)) {
            return '';
        }

        return $timestamp->format($format);
    }
}

if (!function_exists('setting')) {
    /**
     * Get the setting instance.
     *
     * @param $key
     * @param $default
     * @return array|\App\Supports\SettingStore|string|null
     * @author Minh Hao
     */
    function setting($key = null, $default = null)
    {

        if (!empty($key)) {
            try {
                return Setting::get($key, $default);
            } catch (Exception $exception) {
                info($exception->getMessage());
                return $default;
            }
        }
        return SettingFacade::getFacadeRoot();
    }
}


/**
 * @return boolean
 * @author Minh Hao
 */
function check_database_connection()
{
    try {
        DB::connection(config('database.default'))->reconnect();
        return true;
    } catch (Exception $ex) {
        return false;
    }
}
if (! function_exists('array_dot')) {
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array   $array
     * @param  string  $prepend
     * @return array
     */
    function array_dot($array, $prepend = '')
    {
        return Arr::dot($array, $prepend);
    }
}
