<?php

namespace App\ServiceImpl;

use App\Services\UserService;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use App\User;
use Auth;
use App\Group;
use Facade\FlareClient\Report;


class UserServiceImpl implements UserService
{
    /*
    |--------------------------------------------------------------------------
    | Roles user
    |--------------------------------------------------------------------------
    */
    /**
     * @return $query JsonObject
     */
    function getRoles()
    {
        $query = Role::orderBy('created_at', 'DESC')->get();
        return datatables($query)->make(true);
    }

    /**
     * @param $id Integer
     */
    function getRoleById($id)
    {
        return Role::findOrFail($id);
    }

    /**
     * @param $request Request
     */
    function addRole($request)
    {
        $role = Role::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'permission' => json_encode($request->input('permissions')),
            'created_by' => Auth::user()->name
        ]);
    }

    /**
     * @param $id Integer
     * @param $request Request
     */
    function editRole($id, $request)
    {
        $item = Role::findOrFail($id);
        $item->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'permission' => json_encode($request->input('permissions')),
            'created_by' => Auth::user()->name
        ]);
    }

    /**
     * @param $request Request
     */
    function deleteRole($request)
    {
        $item = Role::findOrFail($request->id);
        if ($item) {
            $item->delete();
        }
    }


    /********************************************User*********************************************************************/


    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function ValidateUserEdit($request)
    {
        return $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => [
                'required',
                Rule::unique('users')->ignore($request->id),
            ],
            'email' => [
                'required',
                Rule::unique('users')->ignore($request->id),
            ],
            'avatar' => 'mimes:jpeg,jpg,png|max:20000' // max 20000kb
        ]);
    }

    function ValidateUserPass($request)
    {
        return Validator::make($request->all(), [
            'old_password' => [
                'required', function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail('Old Password didn\'t match');
                    }
                },
            ],
            'password' => 'required|min:6|different:old_password',
            'cf_password' => 'required|min:6|same:password'
        ]);
    }

    function ChangeUser($request)
    {
        $data = User::findOrFail($request->id);
        $params = $request->post();
        unset($params['_token']);
        foreach ($params as $key => $value) {
            if ($request->get($key)) {
                $data->$key = $request->get($key);
            }
        }
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            //Delete file old
            if (Storage::disk('public')->exists('avatar/', $data->avatar)) {
                File::delete('storage/avatar/' . $data->avatar);
            }
            // upload file
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $data->avatar = $avatar;
            \Image::make($file)->save(public_path('storage/avatar/') . $avatar);

        }
        $data->save();
    }

    function ChangePassword($request)
    {
        $user = User::findOrFail($request->id);
        $user->password = Hash::make($request->password);
        $user->save();
    }


    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    */

    /**
     * @return $query JsonObject
     */
    function getUsers()
    {
        $query = User::orderBy('created_at', 'desc')->get();
        return datatables($query)->make(true);
    }

    /**
     * @param $id Integer
     */
    function getUserById($id)
    {
        return User::findOrFail($id);
    }

    /**
     * @param $request Request
     */
    function addUser($request)
    {
        $data = $request->except(['_token']);
        $data['password'] = bcrypt($request->password);
        User::create($data);
    }

    /**
     * @param $id Integer
     * @param $request Request
     */
    function editUser($id, $request)
    {
        $item = User::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
    }

    /**
     * @param $request Request
     */
    function deleteUser($request)
    {
        $item = User::findOrFail($request->id);
        if ($item) {
            $groups = \App\Group::all();
            foreach ($groups as $group) {
                $user_ids = (array)json_decode($group->user_ids);
                if (in_array($item->id, $user_ids)) {
                    $position = array_search($item->id, $user_ids);
                    unset($user_ids[$position]);

                    \App\Group::findOrFail($group->id)
                        ->update([
                            'user_ids' => $user_ids
                        ]);
                }
            }

            $item->delete();
        }
    }

    /*
|--------------------------------------------------------------------------
| Groups
|--------------------------------------------------------------------------
|
*/
    /**
     * @return $query JsonObject
     */
    function getGroups()
    {
        $query = Group::orderBy('created_at', 'DESC');
        return datatables($query)->make(true);
    }

    /**
     * @param $id Integer
     */
    function getGroupById($id)
    {
        return Group::findOrFail($id);
    }

    /**
     * @param $request Request
     */
    function addGroup($request)
    {
        $groups = Group::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_ids' => json_encode(explode(',', $request->input('user_ids')))
        ]);
    }

    /**
     * @param $id Integer
     * @param $request Request
     */
    function editGroup($id, $request)
    {
        $item = Group::findOrFail($id);
        $item->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_ids' => json_encode(explode(',', $request->input('user_ids')))
        ]);
    }

    /**
     * @param $request Request
     */
    function deleteGroup($request)
    {
        $item = Group::findOrFail($request->id);
        if ($item) {
            $item->delete();
        }
    }
}

