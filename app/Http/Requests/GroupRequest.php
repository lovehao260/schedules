<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        if($id){
            $rules = [
                'name' => 'required|unique:groups,name,'.$id.',id',
            ];
        }
        else{
            $rules = [
                'name' =>'required|unique:groups,name',
            ];
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name'=> trans('role.group_name'),
        ];
    }
}
