<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use App\User;
use App\Role;
use Auth;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    public function getList(Request $request)
    {
        return view('front-end.user.index');
    }

    public function getAdd(Request $request)
    {
        $roles = Role::all();
        return view('front-end.user.add', compact('roles'));
    }

    public function postAdd(UserRequest $request)
    {
        $this->userService->addUser($request);
        return redirect()->route('user.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        $roles = Role::all();
        $item = $this->userService->getUserById($id);
        return view('front-end.user.edit',compact('item', 'roles'));
    }

    public function postEdit($id, UserRequest $request)
    {
        $this->userService->editUser($id, $request);
        return redirect()->route('user.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        $user_current_id = Auth::id();
        $user = $this->userService->getUserById($request->id);
        if ($user_current_id == $user->id) {
            return response()->json([
                'status' => 0,
            ]);
        }
        else {
            $this->userService->deleteUser($request);
            return response()->json([
                'status' => 1
            ]);
        }
    }
}
