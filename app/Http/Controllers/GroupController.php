<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Services\UserService;
use App\Group;
use App\User;

class GroupController extends Controller
{ 
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }
    
    public function getIndex(Request $request)
    {
        return view('front-end.group.index');
    }

    public function getAdd(Request $request)
    {
        $users = User::select('name','id')->get();
        return view('front-end.group.add', compact('users'));
    }

    public function postAdd(GroupRequest $request)
    {
        $this->userService->addGroup($request);
        return redirect()->route('group.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        $item = $this->userService->getGroupById($id);
        $user_selected_ids = (array) json_decode($item->user_ids);
        $users = User::select('id', 'name')->get();
        return view('front-end.group.edit',compact('item', 'users', 'user_selected_ids'));
    }

    public function postEdit($id, GroupRequest $request)
    {
        $this->userService->editGroup($id, $request);
        return redirect()->route('group.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        $this->userService->deleteGroup($request);
        return response()->json([
            'status' => 200
        ]);
    }
}
