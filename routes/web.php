<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    //==== Home
    Route::get('/', function () {
        return view('front-end.dashboard.index');
    });

    //==== Roles
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', 'RoleController@getIndex')->name('role.index');
        Route::get('api', 'Api\UserManagementController@getRoles')->name('role.api');
        Route::get('add', 'RoleController@getAdd')->name('role.add');
        Route::post('add', 'RoleController@postAdd')->name('role.post_add');
        Route::get('edit/{id}', 'RoleController@getEdit')->name('role.edit');
        Route::post('edit/{id}', 'RoleController@postEdit')->name('role.post_edit');
        Route::post('delete', 'RoleController@postDelete')->name('role.delete');
    });

    //==== Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@getList')->name('user.index');
        Route::get('/api', 'Api\UserManagementController@getUsers')->name('user.api');
        Route::get('add', 'UserController@getAdd')->name('user.add');
        Route::post('add', 'UserController@postAdd')->name('user.post_add');
        Route::get('edit/{id}', 'UserController@getEdit')->name('user.edit');
        Route::post('edit/{id}', 'UserController@postEdit')->name('user.post_edit');
        Route::post('delete', 'UserController@postDelete')->name('user.delete');
    });

    //==== groups
    Route::group(['prefix' => 'groups'], function () {
        Route::get('/', 'GroupController@getIndex')->name('group.index');
        Route::get('/api', 'Api\UserManagementController@getGroups')->name('group.api');
        Route::get('add', 'GroupController@getAdd')->name('group.add');
        Route::post('add','GroupController@postAdd')->name('group.post_add');
        Route::get('edit/{id}', 'GroupController@getEdit')->name('group.edit');
        Route::post('edit/{id}', 'GroupController@postEdit')->name('group.post_edit');
        Route::post('delete', 'GroupController@postDelete')->name('group.delete');
    });
});
