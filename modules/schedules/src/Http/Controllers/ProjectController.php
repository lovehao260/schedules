<?php


namespace Modules\Schedules\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Modules\Schedules\Models\Link;
use Modules\Schedules\Models\Project;
use Modules\Schedules\Models\Task;
use Modules\Schedules\Repositories\Interfaces\ProjectsInterface;


class ProjectController extends Controller
{

    /**
     * @var ProjectsInterface
     */
    protected $projectRepository;

    /**
     * PageController constructor.
     * @param ProjectsInterface $projectRepository
     * @author Minh Hao
     */
    public function __construct(ProjectsInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     * @author Minh Hao
     */
    public function getIndex()
    {
        if (request()->ajax()) {
            return $this->projectRepository->TableProject();
        }
        return view('schedules-views::projects.index');
    }

    /**
     * @throws \Throwable
     * @author Minh Hao
     */
    public function create()
    {
        $users = User::select('name','id')->get();
        return view('schedules-views::projects.add',compact('users'));
    }

    public function store(Request $request){
        $validator=$this->projectRepository->checkCreatProjects($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $data= new Project();
            $this->projectRepository->createOrUpdate($request,$data);
            return redirect()->route('projects.index')->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.add_success')
            ]);
        }
    }
    function edit ($id){
        $users = User::select('name','id')->get();
        $data = $this->projectRepository->findProject($id);
        $user_selected_ids = (array) json_decode($data->user_ids);
        return view('schedules-views::projects.edit',compact('users','data','user_selected_ids'));
    }
    function update (Request $request){
        $validator=$this->projectRepository->checkEditProjects($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $data = $this->projectRepository->findProject($request->id);
            $this->projectRepository->createOrUpdate($request,$data);
            return redirect()->route('projects.index')->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.add_success')
            ]);
        }
    }

    public function destroy(Request $request){
        $data = $this->projectRepository->findProject($request->id);
        $data->delete();
        return response()->json([
            "action"=> "deleted"
        ]);
    }


    public function detail(){

        $data =$this->projectRepository->findProject(1);
        return view('schedules-views::projects.detail',compact('data'));
    }


    public function apiTaskList(Request $request){
        //$tasks= $this->projectRepository->findTaskOfProject($request->id);
        $tasks = new Task();
        $links = new Link();
        return response()->json([
            "data" => $tasks->all(),
            "links" => $links->all()
        ]);
    }

}
