<?php


namespace Modules\Schedules\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Modules\Schedules\Models\Project;
use Modules\Schedules\Models\Link;
use Modules\Schedules\Repositories\Interfaces\ProjectsInterface;
use Modules\Schedules\Repositories\Interfaces\TaskInterface;

class LinksController extends Controller
{
    /**
     * @var TaskInterface
     */
    protected $taskRepository;

    /**
     * PageController constructor.
     * @param TaskInterface $taskRepository
     * @author Minh Hao
     */
    public function __construct(TaskInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function store(Request $request)
    {
        $link = new Link();
        $link->type = $request->type;
        $link->source = $request->source;
        $link->target = $request->target;
        $link->save();
 
        return response()->json([
            "action"=> "inserted",
            "link-id" => $link->id
        ]);
    }

    public function update($id, Request $request)
    {
        $link = Link::find($id);
        $link->type = $request->type;
        $link->source = $request->source;
        $link->target = $request->target;
        $link->save();
 
        return response()->json([
            "action"=> "updated"
        ]);
    }

    public function destroy($id){
        $link = Link::find($id);
        $link->delete();
        return response()->json([
            "action"=> "deleted"
        ]);
    }

}
