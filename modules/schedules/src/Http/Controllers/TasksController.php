<?php


namespace Modules\Schedules\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Modules\Schedules\Models\Link;
use Modules\Schedules\Models\Project;
use Modules\Schedules\Models\Task;
use Modules\Schedules\Repositories\Interfaces\ProjectsInterface;
use Modules\Schedules\Repositories\Interfaces\TaskInterface;
use Carbon\Carbon;


class TasksController extends Controller
{

    /**
     * @var TaskInterface
     */
    protected $taskRepository;

    /**
     * PageController constructor.
     * @param TaskInterface $taskRepository
     * @author Minh Hao
     */
    public function __construct(TaskInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     * @author Minh Hao
     */
    public function getIndex()
    {

        if (request()->ajax()) {
            return $this->taskRepository->TableTask();
        }
        return view('schedules-views::tasks.index');
    }

    /**
     * @throws \Throwable
     * @author Minh Hao
     */
    public function createProject()
    {
        $users = User::select('name','id')->get();
        return view('schedules-views::tasks.add',compact('users'));
    }
    public function storeProject(Request $request)
    {

        $validator=$this->taskRepository->checkCreatProjects($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {

            $data= new Task();
            $this->taskRepository->createOrUpdate($request,$data);

            return redirect()->route('tasks.index')->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.add_success')
            ]);
        }
    }

    function edit ($id){
        $users = User::select('name','id')->get();
        $data = $this->taskRepository->findProject($id);
        $user_selected_ids = (array) json_decode($data->user_ids);

        return view('schedules-views::tasks.edit',compact('users','data','user_selected_ids'));
    }

    function updateProject (Request $request){

        $validator=$this->taskRepository->checkEditProjects($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        } else {

            $data = $this->taskRepository->findProject($request->id);
            $this->taskRepository->createOrUpdate($request,$data);
            return redirect()->route('tasks.index')->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.update_success')
            ]);
        }
    }

    function apiTaskList(Request $request){
        $data=Task::where(function ($query)  use ($request) {
            $query->where('project_id', '=', $request->id)
                ->orWhere('id', $request->id);
        })->get();
        $links = new Link();
        return response()->json([
            "data" => $data,
            "links" => $links->all()
        ]);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'text' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        $task = new Task();
        $this->taskRepository->saveTask($request, $task);

        return response()->json([
            "action"=> "inserted"
        ]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'text' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        $task = Task::find($id);
        $this->taskRepository->saveTask($request, $task);

        return response()->json([
            "action"=> "updated",
        ]);
    }

    public function destroy($id){
        $task = Task::find($id);
        if($task->childs->count()){
            $remove_ids = $task->childs->pluck('id')->toArray();
            Task::destroy($remove_ids);
        }
        $task->delete();
        return response()->json([
            "action"=> "deleted"
        ]);
    }
    public function detail(Request $request){
        $id=$request->id;
        $project=Task::find($id);
        return view('schedules-views::tasks.detail',compact('id','project'));
    }
    public function getProjectUsers(Request $request){
        $task = Task::find($request->id);
        $user_ids = (array) json_decode($task->user_ids);
        $data = User::select('id', 'name')->whereIn('id', $user_ids)->get()->toArray();
        return response($data);
    }

    /***************-------------------------agile-------------------------------------------------**/
    function agile(){
        $data= Task::where('parent',0)
            ->join('users', 'tasks.auth', '=', 'users.id')
            ->select('users.name as auth_name', 'tasks.*')
            ->get()->toArray();

        $project= $this->CheckUserTask($data);

        return view('schedules-views::agile.project',compact('project'));
    }
    function agileTask (Request $request){
        $id=$request->id;
        $task =Task::join('users', 'tasks.auth', '=', 'users.id')
            ->select('users.name as auth_name', 'tasks.*')
                ->where([
                    'parent'=>$id,
                ])
            ->get()->toArray();


        $tasks = Task::find($id);
        $user_ids = (array) json_decode($tasks->user_ids);
        $users = User::select('id', 'name')->whereIn('id', $user_ids)->get()->toArray();
        $task_open= $this->CheckUserTask($task);
        if (request()->ajax()) {
            $data = $this->taskRepository->findProject($id);
            $user_ids = (array) json_decode($data->user_ids);
            $data_user = User::select('id')->whereIn('id', $user_ids)->get()->toArray();
            return ['data'=>$data,'data_user'=> $data_user];
        }
        $status =    $months = config('config.statusTask');
        $priority =    $months = config('config.priority');
        return view('schedules-views::agile.index',compact('task_open','users','id','user_ids','status','priority'));
    }

    public function CreateAgileTask(Request $request){

        $validator=$this->taskRepository->checkCreatProjects($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $data= new Task();

            $this->taskRepository->createOrUpdate($request,$data);
            return redirect()->back()->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.add_success')
            ]);
        }
    }

    public function updateAgileTask(Request $request){

        $validator=$this->taskRepository->checkEditProjects($request);

        if ($validator->fails()) {
            return back()->with([
                'flash_level'   => 'danger',
                'flash_message' => trans('action.edit_error'),
                'msg'   => 'edit',
                'id'   => $request->id,
            ])
                ->withErrors($validator)
                ->withInput();

        } else {
            $data = $this->taskRepository->findProject($request->id);
            $this->taskRepository->createOrUpdate($request,$data);
            return redirect()->back()->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.edit_success')
            ]);
        }
    }
    public function CheckUserTask($project){
        foreach ( $project as $key => $row){
            if ($row['user_ids']){
                if( !in_array (Auth::id(),(array) json_decode($row['user_ids']))){
                    unset($project[$key]);
                }
            }else{
                unset($project[$key]);
            }
            $row['username']='dddd';

        }
         return $project;
    }
}
