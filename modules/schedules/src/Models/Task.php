<?php

namespace Modules\Schedules\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $appends = ["open"];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent','user_ids', 'project_id','status'
    ];

    public function getOpenAttribute(){
        return true;
    }
    public function childs(){
        return $this->hasMany(Task::class, 'parent')->with('childs');
    }
}
