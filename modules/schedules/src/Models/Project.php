<?php

namespace Modules\Schedules\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name', 'code', 'description','start','end','status','created_at'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author Minh Hao
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
}
