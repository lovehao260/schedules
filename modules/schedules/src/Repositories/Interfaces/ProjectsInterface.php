<?php

namespace Modules\Schedules\Repositories\Interfaces;
use Illuminate\Database\Eloquent\Model;

interface ProjectsInterface
{
    /**
     * @return mixed
     * @author Minh Hao
     */
    public function TableProject();

    /**
     * Get the validation rules that apply to the request.
     * @param $request
     * @return array
     * @author Minh Hao
     */
    public function checkCreatProjects($request);
    /**
     * Create a new model.
     *
     * @param $request
     * @param $data
     * @return false|Model
     * @author Minh Hao
     */
    public function createOrUpdate($request, $data);
    /**
     * @param $id
     * @param array $with
     * @return mixed
     * @author Minh Hao
     */
    public function findProject($id);

    /**
     * Get the validation rules that apply to the request.
     * @param $request
     * @return array
     * @author Minh Hao
     */
    public function checkEditProjects($request);

    /**
     * Get the validation rules that apply to the request.
     * @param $id
     * @return array
     * @author Minh Hao
     */
    public function findTaskOfProject($id);

}
