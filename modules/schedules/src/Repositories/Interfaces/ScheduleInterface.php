<?php

namespace Modules\Schedules\Repositories\Interfaces;
use Illuminate\Database\Eloquent\Model;

interface ScheduleInterface
{

    public function findOrFail($id);
}
