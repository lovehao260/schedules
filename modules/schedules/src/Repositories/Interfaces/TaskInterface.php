<?php

namespace Modules\Schedules\Repositories\Interfaces;
use Illuminate\Database\Eloquent\Model;

interface TaskInterface
{
    /**
     * Get the validation rules that apply to the request.
     * @param $id
     * @return array
     * @author Minh Hao
     */

    public function findTaskOfProject($id);

    public function TableTask();

    public function checkCreatProjects($request);
    public function checkEditProjects($request);
    /**
     * Create a new model.
     *
     * @param $request
     * @param $data
     * @return false|Model
     * @author Minh Hao
     */
    public function createOrUpdate($request, $data);

    /**
     * @param $id
     * @return mixed
     * @author Minh Hao
     */
    public function findProject($id);

    /**
     * Insert or Update Task
     * 
     * @param $request
     * @param lluminate\Database\Eloquent\Model
     * @return void
     * @author Duy Le
     */
    public function saveTask($request, $task);
}
