<?php
namespace Modules\Schedules\Repositories\Eloquement;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Validation\Rule;
use Modules\Schedules\Models\Task;
use Modules\Schedules\Repositories\Interfaces\TaskInterface;
use Carbon\Carbon;

use function GuzzleHttp\json_decode;

class TaskRepository implements TaskInterface
{
    /**
     * @var Eloquent | Model
     */
    protected $model;
    /**
     * RepositoriesAbstract constructor.
     * @param Model|Eloquent $model
     * @author Minh Hao
     */


    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->originalModel = $model;
    }
    public function findProject($id){
        return  $this->model::findOrFail($id);
    }


    public function findTaskOfProject($id){
        return $this->model::where('project_id',$id)->get();
    }

    public function TableTask(){

        return datatables( )->of($this->model::where('parent',0)->orderBy('created_at','desc')->get())
            ->editColumn('code', function ($item) {
                return  anchor_link(route('tasks.detail', $item->id), $item->code);
            })
            ->addColumn('action', function ($data) {
                return table_actions('tasks.edit', 'tasks.delete', $data);
            })
            ->editColumn('start_date', function ($item) {
                return date_from_database($item->start_date);
            })
            ->editColumn('progress', function ($item) {
                return table_progress($item->progress);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })

            ->rawColumns(['code','created_at','action','progress','start_date'])
            ->make(true);
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function checkCreatProjects($request){
;
        return $validator = Validator::make($request->all(), [
            'text' => 'required',
            'code' => 'required|unique:tasks|max:10|regex:/^[a-zA-Z-0-9#]+$/u',
            'start_date' => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                    $request= Request();
                    if($request->parent){
                        $project= Task::findOrFail($request->parent);
                        if( $value < $project->start_date){
                            $fail('Time start of task can not smaller time start of Project' );
                        }
                    }
                },
            ],
            'end_date' => 'required|date|after:start_date|',
            'user_ids' => 'required',

        ]);

    }
    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function checkEditProjects($request){

        return $validator = Validator::make($request->all(), [
            'text' => 'required',
            'code' => [
                'required',
                Rule::unique('tasks')->ignore($request->id),
                'max:10',
                'regex:/^[a-zA-Z-0-9#]+$/u'
            ],
            'start_date' => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                    $request= Request();
                    if($request->parent){
                        $project= Task::findOrFail($request->parent);
                        if( $value < $project->start_date){
                            $fail('Time start of task can not smaller time start of Project' );
                        }
                    }
                },
            ],
            'end_date' => 'required|date|after:start_date|',
            'user_ids' => 'required',
        ]);
    }

    /**
     * Create a new model.
     *
     * @param $request
     * @param $data
     * @return void
     * @author Minh Hao
     */
    public function createOrUpdate($request, $data)
    {

        $params = $request->post();
        CarbonPeriod::macro('countWeekdays', function () {
            return $this->filter('isWeekday')->count();
        });
        unset($params['_token']);
        foreach ($params as $key => $value) {
            if ($request->get($key)) {
                if($key=="user_ids"){
                    if($request->parent){
                        $data->user_ids=json_encode($request->user_ids);
                    }else{

                        $user_ids = explode(',', $request->get($key));
                        $user_ids = json_encode(array_filter($user_ids)); //remove empty and duplicate id
                        $data->user_ids = $user_ids;
                    }
                }
                else{
                    $data->$key = $request->get($key);
                }


            }
        }
        $data->auth = Auth::id();
        $data->duration=CarbonPeriod::create($params['start_date'], $params['end_date'])->countWeekdays();
        unset($data['user_id']);
        if($request->status ==0 && $request->status ){
            $data->status = $request->status  ;
        }
        if($request->progress ==1){
            $data->status = 2 ;
        }
        if($request->priority ==0 && $request->status){
            $data->priority = $request->priority;
        }

        $data->save();

    }

    /**
     * Insert or Update Task
     *
     * @param $request
     * @param lluminate\Database\Eloquent\Model
     * @return void
     * @author Duy Le
     */
    public function saveTask($request, $task)
    {
        $task->text = $request->text;
        $task->description = $request->description;
        $task->start_date = $request->start_date;
        $task->end_date = $request->end_date;
        $task->duration = $request->duration;
        $task->progress = $request->has("progress") ? $request->progress : 0;
        $task->parent = $request->parent;
        $task->auth = Auth::id();
        $task->user_ids = $request->user_ids;
        $task->priority = $request->priority;
        $task->project_id = $request->project_id;
        if($request->progress == 0){
            $task->status = 0;
        }
        else if($request->progress == 1){
            $task->status = 2;
        }
        else{
            $task->status = 1;
        }
        
        $task->save();
    }

}
