<?php
namespace Modules\Schedules\Repositories\Eloquement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Schedules\Models\Project;
use Modules\Schedules\Models\Task;
use Modules\Schedules\Repositories\Interfaces\ProjectsInterface;


class ProjectRepository implements ProjectsInterface
{
    /**
     * @var Eloquent | Model
     */
    protected $model;
    /**
     * RepositoriesAbstract constructor.
     * @param Model|Eloquent $model
     * @author Minh Hao
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->originalModel = $model;
    }
    public function findProject($id){
        return  $this->model::findOrFail($id);
    }
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Minh Hao
     */
    function TableProject()
    {

        return datatables()->of($this->model::latest()->orderBy('created_at','desc')->get())
            ->editColumn('code', function ($item) {
                return  anchor_link(route('projects.detail', $item->id), $item->code);
            })
            ->addColumn('action', function ($data) {
                return table_actions('projects.edit', 'projects.delete', $data);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('status', function ($item) {
                return table_status($item->status);
            })
            ->rawColumns(['code','action','created_at','status'])
            ->make(true);
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function checkCreatProjects($request){
        return $validator = Validator::make($request->all(), [
            'name' => 'required |max:25',
            'code' => 'required|unique:projects|max:10|regex:/^[a-zA-Z-0-9]+$/u',
            'start' => 'required|date|after:yesterday',
            'end' => 'required|date|after:start|',
        ]);
    }
    /**
     * Create a new model.
     *
     * @param $request
     * @param $data
     * @return void
     * @author Minh Hao
     */
    public function createOrUpdate($request, $data)
    {
        $params = $request->post();
        unset($params['_token']);
        foreach ($params as $key => $value) {
            if ($request->get($key)) {
                $data->$key = $request->get($key);
            }
        }
        $data->save();
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function checkEditProjects($request){

        return $validator = Validator::make($request->all(), [
            'name' => 'required |max:25',
            'code' => [
                'required',
                Rule::unique('projects')->ignore($request->id),
                'max:10',
                'regex:/^[a-zA-Z-0-9]+$/u'
            ],
            'start' => 'required|date',
            'end' => 'required|date|after:start|',
        ]);
    }

    public function findTaskOfProject($id){
     return Task::where('project_id',$id)->get();
    }

}
