<?php

namespace Modules\Schedules\Providers;
use Illuminate\Support\ServiceProvider;
use Modules\Schedules\Models\Project;
use Modules\Schedules\Models\Task;
use Modules\Schedules\Repositories\Eloquement\ProjectRepository;
use Modules\Schedules\Repositories\Eloquement\ScheduleRepository;
use Modules\Schedules\Repositories\Eloquement\TaskRepository;
use Modules\Schedules\Repositories\Interfaces\ProjectsInterface;
use Modules\Schedules\Repositories\Interfaces\ScheduleInterface;
use File;
use Modules\Schedules\Repositories\Interfaces\TaskInterface;

class MainServiceProviders extends ServiceProvider
{
    public function boot()
    {

        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'schedules-views');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'schedules-lang');
        $this->mergeConfigFrom(__DIR__ . '/../../config/general.php', 'config');
        // Load the configurations from `Modules/Ticket/TicketConfig.php`



    }
        public function register()
    {
        $this->app->singleton(ScheduleInterface::class, function () {
            return new ScheduleRepository();
        });
        $this->app->singleton(ProjectsInterface::class, function () {
            return new ProjectRepository(new Project());
        });
        $this->app->singleton(TaskInterface::class, function () {
            return new TaskRepository(new Task());
        });
       $this->autoload(__DIR__ . '/../../helpers');
    }
    public static function autoload($directory)
    {
        $helpers = File::glob($directory . '/*.php');
        foreach ($helpers as $helper) {
            File::requireOnce($helper);
        }
    }
}
