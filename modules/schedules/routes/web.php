<?php


use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {


});
Route::group(['namespace' => 'Modules\Schedules\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(['prefix' => config('modules.schedules.general.admin_dir'), 'middleware' => 'auth'], function () {
//        Route::group(['prefix' => 'projects'], function () {
//            Route::get('/', [
//                'as' => 'projects.index',
//                'uses' => 'ProjectController@getIndex',
//            ]);
//            Route::get('/add', [
//                'as' => 'projects.create',
//                'uses' => 'ProjectController@create',
//            ]);
//            Route::post('/post-add', [
//                'as' => 'projects.store',
//                'uses' => 'ProjectController@store',
//            ]);
//            Route::get('/edit/{id}', [
//                'as' => 'projects.edit',
//                'uses' => 'ProjectController@edit',
//            ]);
//            Route::post('/update/{id}', [
//                'as' => 'projects.update',
//                'uses' => 'ProjectController@update',
//            ]);
//            Route::get('/delete/{id}', [
//                'as' => 'projects.delete',
//                'uses' => 'ProjectController@destroy',
//            ]);
//
//            Route::get('/detail/{id}', [
//                'as' => 'projects.detail',
//                'uses' => 'ProjectController@detail',
//            ]);
//
//        });


        Route::group(['prefix' => 'task'], function () {
            Route::get('/', [
                'as' => 'tasks.index',
                'uses' => 'TasksController@getIndex',
            ]);
            Route::get('/add', [
                'as' => 'tasks.create',
                'uses' => 'TasksController@createProject',
            ]);
            Route::post('/post-add', [
                'as' => 'tasks.createProject',
                'uses' => 'TasksController@storeProject',
            ]);
            Route::get('/edit/{id}', [
                'as' => 'tasks.edit',
                'uses' => 'TasksController@edit',
            ]);
            Route::post('/update/{id}', [
                'as' => 'tasks.update.project',
                'uses' => 'TasksController@updateProject',
            ]);
            Route::get('/{id}', [
                'as' => 'tasks.delete',
                'uses' => 'TasksController@destroy',
            ]);
            Route::get('/detail/{id}', [
                'as' => 'tasks.detail',
                'uses' => 'TasksController@detail',
            ]);

            Route::group(['prefix' => 'api'], function () {
                Route::get('/project-task/{id}', [
                    'as' => 'projects.api.task',
                    'uses' => 'TasksController@apiTaskList',
                ]);
                Route::get('/project-users/{id}', [
                    'as' => 'projects.api.users',
                    'uses' => 'TasksController@getProjectUsers',
                ]);
                Route::post('/task', [
                    'as' => 'tasks.store',
                    'uses' => 'TasksController@store',
                ]);
                Route::put('/task/{id}', [
                    'as' => 'tasks.update',
                    'uses' => 'TasksController@update',
                ]);
                Route::delete('/task/{id}', [
                    'as' => 'tasks.destroy',
                    'uses' => 'TasksController@destroy',
                ]);
                Route::post('/link', [
                    'as' => 'links.store',
                    'uses' => 'LinksController@store',
                ]);
                Route::put('/link/{id}', [
                    'as' => 'links.update',
                    'uses' => 'LinksController@update',
                ]);
                Route::delete('/link/{id}', [
                    'as' => 'links.destroy',
                    'uses' => 'LinksController@destroy',
                ]);
            });
        });
        Route::group(['prefix' => 'agile'], function () {
            Route::get('/', [
                'as' => 'agile.project',
                'uses' => 'TasksController@agile',
            ]);
            Route::get('/{id}', [
                'as' => 'agile.task',
                'uses' => 'TasksController@agileTask',
            ]);

            Route::get('detail/{id}', [
                'as' => 'agile.detail',
                'uses' => 'TasksController@agileTask',
            ]);
                Route::post('agile/create', [
                    'as' => 'agile.create',
                    'uses' => 'TasksController@CreateAgileTask',
                ]);
                Route::post('update/{id}', [
                    'as' => 'agile.update',
                    'uses' => 'TasksController@updateAgileTask',
                ]);
        });
    });
});
