<?php
use IlluminateAgnostic\Arr\Support\Arr;
if (!function_exists('table_actions')) {
    /**
     * @param $edit
     * @param $delete
     * @param $item
     * @param string $extra
     * @return string
     * @throws Throwable
     * @author Minh Hao
     */
    function table_actions($edit, $delete, $item, $extra = null)
    {
        return view('schedules-views::items.actions', compact('edit', 'delete', 'item', 'extra'))->render();
    }
}
if (!function_exists('table_status')) {
    /**
     * @param $selected
     * @param array $statuses
     * @return string
     * @internal param $status
     * @internal param null $activated_text
     * @internal param null $deactivated_text
     * @author Minh Hao
     * @throws Throwable
     */
    function table_status($selected, $statuses = [])
    {
        if (empty($statuses) || !is_array($statuses)) {

            $statuses = [
                0 => [
                    'class' => 'status   badge-primary',
                    'text' => trans('schedules-lang::tables.processing'),
                ],
                1 => [
                    'class' => 'status   badge-success',
                    'text' => trans('schedules-lang::tables.success'),
                ],

                $statuses < 1 => [
                    'class' => ' status   badge-danger',
                    'text' => trans('schedules-lang::tables.cancel'),
                ],
            ];
        }
        return view('schedules-views::items.status', compact('selected', 'statuses'))->render();
    }
}


if (!function_exists('table_progress')) {
    /**
     * @param  $progress
     * @return string
     * @internal param $status
     * @internal param null $activated_text
     * @internal param null $deactivated_text
     * @author Minh Hao
     * @throws Throwable
     */
    function table_progress($progress)
    {
        if (empty($progress) || !is_array($progress)) {
            if($progress == 1){
                $progress =  [
                    'class' => 'btn-success',
                    'text' => trans('schedules-lang::tables.success'),
                ];
            }else{
                $progress =  [
                    'class' => 'btn-info',
                    'text' => $progress * 100 ."%",
                ];
            }
        }
        return view('schedules-views::items.progress', compact('progress'))->render();
    }
}

if (!function_exists('anchor_link')) {
    /**
     * @param $link
     * @param $item
     * @param array $options
     * @return string
     * @author Sang Nguyen
     * @throws Throwable
     */
    function anchor_link($link, $item, array $options = [])
    {
        return view('schedules-views::items.link', compact('link', 'item', 'options'))->render();
    }
}

if (! function_exists('config')) {
    /**
     * Get / set the specified configuration value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed|\Illuminate\Config\Repository
     */
    function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('config');
        }

        if (is_array($key)) {
            return app('config')->set($key);
        }

        return app('config')->get($key, $default);
    }
}
if (! function_exists('array_get')) {
    /**
     * Get an item from an array using "dot" notation.
     *
     * @param  \ArrayAccess|array  $array
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}
