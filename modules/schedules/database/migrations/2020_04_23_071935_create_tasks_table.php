<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->default(0);
            $table->string('text');
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->integer('duration')->nullable();
            $table->float('progress')->default(0);
            $table->string('user_ids')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('parent')->default(0);
            $table->integer('auth');
            $table->tinyInteger('priority')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
