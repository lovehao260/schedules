<?php


return [
    'create' => 'Create project',
    'creat_title' => 'Create new a project',
    'edit' =>'Edit project',
    'edit_title' => 'Edit project data',
    'code' => 'Project code',
    'time' => 'Time Line',
    'name' => 'Project name',
    'create_task' => 'Create new a task',
    'update_task' => 'Update new a task',
];
