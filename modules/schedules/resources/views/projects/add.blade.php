@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="content">
            <div class="container-fluid">
                <form role="form" method="POST" action="{{ route('projects.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">{{ trans('schedules-lang::project.creat') }}</h4>
                                    <p class="card-category">{{ trans('schedules-lang::project.creat_title') }}</p>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div
                                                    class="form-group bmd-form-group @error('name') has-danger @enderror ">
                                                    <label class="bmd-label-floating">{{__('group.name')}}</label>
                                                    <input type="text" class="form-control" name="name"
                                                           value="{{ old('name') }}">
                                                    @error('name')<span
                                                        class="text-danger">{{ $message }}</span>@enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div
                                                    class="form-group bmd-form-group @error('code') has-danger @enderror ">
                                                    <label class="bmd-label-floating">{{ trans('schedules-lang::project.code') }}</label>
                                                    <input type="text" class="form-control" name="code"
                                                           value="{{ old('code') }}">
                                                    @error('code')<span
                                                        class="text-danger">{{ $message }}</span>@enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group bmd-form-group">
                                                    <label
                                                        class="bmd-label-floating">{{__('group.description')}}</label>
                                                    <textarea class="form-control" rows="2"
                                                              name="description">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group"><label
                                                    class="bmd-label-floating">{{ trans('schedules-lang::project.time') }}</label>
                                                <div class="form-group bmd-form-group">
                                                    {{__('activity.from')}}: <input readonly id="from-date" name="start"
                                                                                    value="{{old('start')}}">
                                                    - {{__('activity.to')}}: <input readonly id="to-date" name="end"
                                                                                    value="{{old('end')}}">
                                                    <a href="" id="reset"><i class="material-icons">replay</i></a>
                                                    <div>

                                                    </div>
                                                    @error('start')<span
                                                        class="text-danger">{{ $message }}</span>@enderror
                                                    @error('end')<span
                                                        class="text-danger" style="margin-left: 70px">{{ $message }}</span>@enderror
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>{{__('group.select_user')}}:</label>
                                                <div class="form-group bmd-form-group">

                                                    <select id="select_user" class="select-user-box"
                                                            data-placeholder="{{__('group.select_user')}}"
                                                            style="width: 50%">
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">
                                                                {{ $user->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <input name="user_ids" value="" hidden/>
                                                    <a href="" id="adduser">ADD</a>
                                                    <ul class="list_user"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit"
                                            class="btn btn-primary pull-right">{{__('action.save')}}</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <!--Jquery ui -->
    <script src="{{ asset('lib/jquery/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>


        //Initialize Select2 Elements
        $('.select-user-box').select2({
            theme: "classic"
        });

        //list user_id selected
        let arr_ids = [];
        let user_ids_element = $("input[name='user_ids']");

        $("#adduser").click(function (e) {
            e.preventDefault();
            let name = "";
            let id = $("#select_user").val();
            if (id && arr_ids.indexOf(id) < 0) {
                arr_ids.push(id);
                user_ids_element.val(arr_ids);
                name = $("#select_user option:selected").text();
                $('.list_user').append(`<li id="item_${id}"><a onClick="removeItem(${id})">x</a>${name}</li>`);
            }

        });

        function removeItem(id) {
            let pos = arr_ids.indexOf(id + "");
            if (pos > -1) {
                $("#item_" + id).remove();
                arr_ids.splice(pos, 1);
                user_ids_element.val(arr_ids);
            }

        }


        $('#from-date').datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 9999);
                }, 0);
            },
            onSelect: function (dateText) {

                $('#list-item').DataTable().destroy();
            }
        });

        $('#to-date').datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 9999);
                }, 0);
            },
            onSelect: function (dateText) {

                $('#list-item').DataTable().destroy();
            },
            // minDate: null
        });

        $("#reset").on('click', function (e) {
            e.preventDefault();

            $("#from-date, #to-date").val('');
            $('#list-item').DataTable().destroy();

        });
    </script>

@endpush
