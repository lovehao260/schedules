@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('projects.create') }}" class="btn btn-primary float-right"><i
                                class="fa fa-plus"></i> {{ trans('schedules-lang::tables.creat') }}</a>
                    </div>
                    <div class="card-body">
                        <table id="list-project" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('schedules-lang::tables.name') }}</th>
                                <th>{{ trans('schedules-lang::tables.code') }}</th>
                                <th>{{ trans('schedules-lang::tables.description') }}</th>
                                <th>{{ trans('schedules-lang::tables.start') }}</th>
                                <th>{{ trans('schedules-lang::tables.end') }}</th>
                                <th>{{ trans('schedules-lang::tables.status') }}</th>
                                <th>{{ trans('schedules-lang::tables.created_at') }}</th>
                                <th style="width: 10%;">{{ trans('schedules-lang::tables.operations') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('script')
    <!-- DataTables -->
    <script>
        @if(session('language')=='ja')
            url = "{{asset('lib/datatables/table.json')}}";
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $('#list-project').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url: "{{ route('projects.index') }}",
            },
            columns:[
                {
                    data: 'stt',
                    name: 'stt'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'code',
                    name: 'code'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'start',
                    name: 'start'
                },
                {
                    data: 'end',
                    name: 'end'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ],
            order: [[ 6, "desc" ]],

        });

    </script>
@endpush
