@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <list-task-component :project="{{$id}}" v-bind:end_projects="{{$project}}" ></list-task-component>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('page-scripts')
    <script src="{{asset('js/schedules.js')}}"></script>
@endpush
