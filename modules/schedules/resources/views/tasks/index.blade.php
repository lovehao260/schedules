@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('tasks.create') }}" class="btn btn-primary float-right"><i
                                class="fa fa-plus"></i> {{ trans('schedules-lang::tables.create') }}</a>
                    </div>
                    <div class="card-body ">
                        <table id="list-project" class="table table-bordered  table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('schedules-lang::tables.code') }}</th>
                                <th>{{ trans('schedules-lang::tables.name') }}</th>
                                <th>{{ trans('schedules-lang::tables.description') }}</th>
                                <th>{{ trans('schedules-lang::tables.start') }}</th>
                                <th>{{ trans('schedules-lang::tables.duration') }}</th>
                                <th>{{ trans('schedules-lang::tables.status') }}</th>
                                <th>{{ trans('schedules-lang::tables.created_at') }}</th>
                                <th>{{ trans('schedules-lang::tables.operations') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('script')
    <!-- DataTables -->
    <script>
        @if(session('language')=='ja')
            url = "{{asset('lib/datatables/table.json')}}";
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $('#list-project').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url: "{{ route('tasks.index') }}",
            },
            columns:[

                {
                    data: 'code',
                    name: 'code',

                },
                {
                    data: 'text',
                    name: 'text'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'start_date',
                    name: 'start_date'
                },
                {
                    data: 'duration',
                    name: 'duration'
                },

                {
                    data: 'progress',
                    name: 'progress'
                },
                {
                    data: 'created_at',
                    name: 'created_at',

                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },

            ],
            order: [],
            "createdRow": function( row, data, dataIndex){
                if(data.priority === '1' && data.status < 2){
                    // $(row).addClass('redClass');
                }
            }
        });

    </script>
@endpush
