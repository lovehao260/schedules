<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">{{ trans('schedules-lang::project.code') }}</label>
            <div
                class="form-group bmd-form-group @error('text') has-danger @enderror ">

                <input type="text" class="form-control" name="text"
                       value="{{ old('text') }}">
                @error('text')<span
                    class="text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="form-group">
            <label
                class="bmd-label-floating">{{__('group.description')}}</label>
            <div class="form-group bmd-form-group">

                <textarea class="form-control" rows="2"
                          name="description">{{ old('description') }}</textarea>
            </div>
        </div>
        <div class="form-group"><label
                class="bmd-label-floating">{{ trans('schedules-lang::project.time') }}</label>
            <div class="form-group bmd-form-group">
                {{__('activity.from')}}: <input readonly class="from-date" name="start_date"
                                                value="{{old('start_date')}}">
                - {{__('activity.to')}}: <input readonly class="to-date" name="end_date"
                                                value="{{old('end_date')}}">

                <div>

                </div>
                @error('start_date')<span
                    class="text-danger">{{ $message }}</span>@enderror
                @error('end_date')<span
                    class="text-danger" style="margin-left: 70px">{{ $message }}</span>@enderror
            </div>
        </div>
        @if(isset($users))
            <div class="form-group">

                <label>{{__('group.select_user')}}:</label>
                <div class="form-group bmd-form-group">
                    <ul class="list-user" >
                        @foreach($users as $row)
                        <li><input type="checkbox" name="user_ids[]" value="{{$row['id']}}"> {{$row['name']}}</li>
                        @endforeach
                    </ul>

                </div>
            </div>
        @endif
    </div>

</div>
@error('user_ids')<span
    class="text-danger">{{$message}}</span>@enderror
<style>
    .modal-lg {
        max-width: 80% !important;
    }
    .list-user li {
        display: inline;
        margin-left: 20px;
    }
</style>
