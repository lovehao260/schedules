<form id="form_edit" role="form" method="POST" action="" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        @include('schedules-views::items.form_add')
        <input type="text" class="form-control" name="parent" hidden
               value="{{ $id }}">
        <div class="form-group row">
            <div class="col-6">
                <label
                    class="bmd-label-floating">Trang Thai</label>
                <div class="form-group bmd-form-group">
                    <select id="Status" name="status">
                        <optgroup label="Status">
                            @foreach ($status as $key => $value)
                                <option  value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <label
                    class="bmd-label-floating">Priority</label>
                <div class="form-group bmd-form-group">
                    <select id="priority" name="priority">
                        <optgroup label="Priority">
                            @foreach ($priority as $key => $value)
                                <option  value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

        <div id="audio-player-container" >
            <div class="audio-progress" id="audio-progress">
                <div id="draggable-point" style="position:absolute;" class="draggable ui-widget-content">
                    <div id="audio-progress-handle"></div>
                </div>
                <div id="audio-progress-bar" class="bar">

                </div>
            </div>
        </div>
        <div id="posX"></div>
        <input value="" name="progress" id="progress" hidden >
    </div>
    <div class="modal-footer">
        <button type="button" onclick="clearSeession()" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
        <button type="submit"  class="btn btn-primary">{{ trans('schedules-lang::tables.update') }}</button>
    </div>

</form>
