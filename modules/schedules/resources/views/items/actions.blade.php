<div class="table-actions">
    {!! $extra !!}
    @if (!empty($edit))
        <a href="{{ route($edit, $item->id) }}" class="btn btn-icon btn-sm btn-primary tip"
           data-original-title="{{ trans('schedules-lang::tables.edit') }}"><i class="fa fa-edit"></i></a>
    @endif

    @if (!empty($delete))
        <a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog tip" data-toggle="modal" data-target="#delete-crud-modal"
           data-section="{{ route($delete, $item->id) }}" role="button"
           data-original-title="{{ trans('schedules-lang::tables.delete') }}">
            <i class="fa fa-trash"></i>
        </a>
    @endif

<!-- confirm dialog-->
    <div class="modal fade" id="delete-crud-modal" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <strong>{{__('action.delete_confirm')}}</strong>
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure')}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmCancel" class="btn btn-default btn-cancel"
                            data-dismiss="modal">
                        {{__('action.cancel')}}
                    </button>
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok delete-crud-entry" >
                        {{__('action.delete')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>

<script>
    $(document).on('click', '.deleteDialog', (event) => {
        event.preventDefault();
        let _self = $(event.currentTarget);
        $('.delete-crud-entry').data('section', _self.data('section')).data('parent-table', _self.closest('.table').prop('id'));
        $('.modal-confirm-delete').modal('show');

    });
    $('.delete-crud-entry').on('click', (event) => {
        event.preventDefault();
        let _self = $(event.currentTarget);
        $('.modal-confirm-delete').modal('hide');
        let deleteURL = _self.data('section');
        $.ajax({
            url: deleteURL,
            type: 'GET',
            success: (data) => {
                location.reload();
            },
        });
    });
</script>
