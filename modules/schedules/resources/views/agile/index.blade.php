@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="flex-1 breadcrumb">
                                <h5 class=" breadcrumb-1 w-75"> Open(4)</h5>
                                <div class="time-0 text-right w-25"> Open(4)</div>
                            </div>
                            <?php $time=0 ; $time1=0 ; $time2=0 ?>
                            @foreach($task_open as $row)

                                @if($row['status']===0)
                                    <div class="agile-item agile-item-1" onclick="editStatus({{$row['id']}})">
                                        <div class="d-flex bd-highlight">
                                            <div class=" flex-grow-1 bd-highlight">
                                                <div class="item-name">
                                                    {{$row['text']}}
                                                </div>
                                            </div>
                                            <div class="p-2 bd-highlight">{{$row['duration']}}day
                                                ({{$row['duration']*8}}h)
                                            </div>
                                        </div>
                                        <div class="item-title">
                                            {{$row['description']}}
                                        </div>
                                        <div class="item-auth">
                                            Auth: {{$row['auth_name']}}
                                        </div>
                                        <div class="progress-bar progress-bar-striped progress-bar-animated"
                                             style="width:{{$row['progress']*100}}%">
                                               <?php $time+=$row['duration']; ?>
                                        </div>
                                    </div>
                                @endif

                            @endforeach
                            <input id="duration_time" value="{{$time}}" hidden  />
                            <button type="button" class="btn btn-primary userinfo" data-toggle="modal"
                                 data-project="{{$time}}"   data-target="#task-modal" data-whatever="@mdo">Add Task
                            </button>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="flex-1 breadcrumb">
                                <h5 class=" breadcrumb-2 w-75"> Open(4)</h5>
                                <div class="time-1 text-right w-25"> Open(4)</div>
                            </div>

                            @foreach($task_open as $row)
                                @if($row['status']===1)
                                    <div class="agile-item  agile-item-2"  onclick="editStatus({{$row['id']}})">
                                        <div class="d-flex bd-highlight">
                                            <div class=" flex-grow-1 bd-highlight">
                                                <div class="item-name">
                                                    {{$row['text']}}
                                                </div>
                                            </div>
                                            <div class="p-2 bd-highlight">{{$row['duration']}}day
                                                ({{$row['duration']*8}}h)
                                            </div>
                                        </div>
                                        <div class="item-title">
                                            {{$row['description']}}
                                        </div>
                                        <div class="item-auth">
                                            Auth: {{$row['auth_name']}}
                                        </div>
                                        <div class="progress-bar progress-bar-striped progress-bar-animated"
                                             style="width:{{$row['progress']*100}}%">
                                            {{$row['progress']*100}}%
                                        </div>
                                    </div>
                                    <?php $time1+=$row['duration']; ?>
                                @endif
                            @endforeach
                            <input id="duration_time1" value="{{$time1}}" hidden  />
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="flex-1 breadcrumb">
                                <h5 class=" breadcrumb-3 w-75"> Open(4)</h5>
                                <div class="time-2 text-right w-25"> Open(4)</div>
                            </div>

                            @foreach($task_open as $row)
                                @if($row['status']===2)
                                    <div class="agile-item  agile-item-3" onclick="editStatus({{$row['id']}})">
                                        <div class="d-flex bd-highlight">
                                            <div class="flex-grow-1 bd-highlight">
                                                <div class="item-name">
                                                    {{$row['text']}}
                                                </div>
                                            </div>
                                            <div class="p-2 bd-highlight">{{$row['duration']}}day
                                                ({{$row['duration']*8}}h)
                                            </div>
                                        </div>
                                        <div class="item-title">
                                            {{$row['description']}}
                                        </div>
                                        <div class="item-auth">
                                            Auth: {{$row['auth_name']}}
                                        </div>
                                        <div class="progress-bar progress-bar-striped progress-bar-animated"
                                             style="width:{{$row['progress']*100}}%">
                                            {{$row['progress']*100}}%
                                        </div>
                                    </div>
                                    <?php $time2+=$row['duration']; ?>
                                @endif
                            @endforeach
                            <input id="duration_time2" value="{{$time2}}" hidden  />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="task-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mw-100 w-50" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ trans('schedules-lang::project.create_task') }} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="POST" action="{{ route('agile.create') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="modal-body">
                                @include('schedules-views::items.form_add')
                            <input type="text" class="form-control" name="parent" hidden
                                   value="{{ $id }}">
                            <input type="text" class="form-control" name="project_id" hidden
                                   value="{{ $id }}">
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary " onclick="clearSeession()" data-dismiss="modal">{{__('action.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('schedules-lang::tables.create') }}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="task-modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mw-100 w-50" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ trans('schedules-lang::project.update_task') }} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @include('schedules-views::items.edit_form_task')
            </div>
        </div>
    </div>

    <style>
        .agile-item {
            margin-bottom: 20px;
            padding: 15px;
            background: white;
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
        }
        .agile-item:hover{
            transform: scale(1.03);
            background: #bdb78d;
            color: white;
        }

        .agile-item .item-title, .item-name {
            font-weight: bold;
        }
        .txt_name{
            display: none;
        }
        .form-group {
            margin-bottom: 0;
        }
        .audio-progress {
            height: 1rem;
            width: 100%;
            background-color: #C0C0C0;
            position: relative;
            display: flex;
            border-radius: 5px;
            text-align: center;
            font-size: 14px;
        }
        .audio-progress .bar {
            height: 100%;
            background-color: #E95F74;
        }

        #audio-progress-handle {
            display: block;
            position: absolute;
            z-index: 1;
            margin-top: -5px;
            top: 0 !important;
            margin-left: -10px;
            width: 20px;
            height: 20px;
            border: 4px solid #D3D5DF;
            border-top-color: #D3D5DF;
            border-right-color: #D3D5DF;
            transform: rotate(45deg);
            border-radius: 100%;
            background-color: #fff;
            box-shadow: 0 1px 6px rgba(0, 0, 0, .2);
            cursor: pointer;
        }

        .draggable {
            float: left;
        }
        .draggable-point{
            top: 0 !important;
        }
        .done{
            background-color: #0ca30a !important;
            color: white;
        }
    </style>
@endsection
@push('script')
    <!--Jquery ui -->
    <script src="{{ asset('lib/jquery/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>


        $('#draggable-point').draggable({
            drag: function() {
                let offset = $(this).offset();
                let  xPos = (100 * parseFloat($(this).css("left"))) / (parseInt($(this).parent().css("width"))) ;

                $('#audio-progress-bar').css({
                    'width':   Math.round(xPos)+ "%",
                    'top':0
                });
                $('#audio-progress-bar').text(  Math.round(xPos)+ "%");

                $('#progress').val(  Math.round(xPos)/100);
                if(Math.round(xPos)==100){
                    $('.audio-progress .bar').addClass('done')
                }else{
                    $('.audio-progress .bar').removeClass('done')
                }
            }
        });
        //Initialize Select2 Elements
        $('.select-user-box').select2({
            theme: "classic"
        });


        //list user_id selected
        let arr_ids = [];
        let user_ids_element = $("input[name='user_ids']");

        $("#adduser").click(function (e) {
            e.preventDefault();
            let name = "";
            let id = $(".select_user").val();
            if (id && arr_ids.indexOf(id) < 0) {
                arr_ids.push(id);
                user_ids_element.val(arr_ids);
                name = $(".select_user option:selected").text();
                $('.list_user').append(`<li id="item_${id}"><a onClick="removeItem(${id})">x</a>${name}</li>`);
            }

        });

        function removeItem(id) {
            let pos = arr_ids.indexOf(id + "");
            if (pos > -1) {
                $("#item_" + id).remove();
                arr_ids.splice(pos, 1);
                $user_ids_element.val(arr_ids);
            }

        }


        $('.from-date ').datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 9999);
                }, 0);
            },
            onSelect: function (dateText) {

                $('#list-item').DataTable().destroy();
            }
        });

        $('.to-date ').datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 9999);
                }, 0);
            },
            onSelect: function (dateText) {

                $('#list-item').DataTable().destroy();
            },
            // minDate: null
        });

        $("#reset").on('click', function (e) {
            e.preventDefault();

            $(".from-date , .to-date ").val('');
            $('#list-item').DataTable().destroy();

        });
        @if (count($errors) > 0)
            @if(Session::has('msg') == 'edit')
              $('#form_edit').attr('action',"{{url('/')}}/agile/update/"+{{Session::get('id')}} );
              $('#task-modal-edit').modal('show');
             @else
             $('#task-modal').modal('show');
             @endif
        @endif

    </script>
    <script>
        $( document ).ready(function() {
          let total_op=  $('.agile-item-1').length;
            let total_op1=  $('.agile-item-2').length;
            let total_op2=  $('.agile-item-3').length;
            let time = $('#duration_time').val();
            let time1 = $('#duration_time1').val();
            let time2 = $('#duration_time2').val();
            $( ".breadcrumb-1" ).text( "Open (" + total_op + ")");
            $( ".time-0" ).text(  time + "day ("+time*8+"h)");

            $( ".breadcrumb-2" ).text( "In Progress (" + total_op1 + ")");
            $( ".time-1" ).text(  time1 + "day ("+time1*8+"h)");

            $( ".breadcrumb-3" ).text( "Done (" + total_op2 + ")");
            $( ".time-2" ).text(  time2 + "day ("+time2*8+"h)");

        });

        function editStatus(id) {

            $("#task-modal").hide();
            $.ajax({
                type: 'get',
                route:"agile.task",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function(data) {
                    $("#task-modal-edit").modal('show');
                    $('#form_edit').attr('action',"{{url('/')}}/agile/update/"+id );
                    $("#task-modal-edit input[name='text']").val(data.data.text);
                    $("#task-modal-edit input[name='description']").val(data.data.description);
                    $("#task-modal-edit input[name='start_date']").val(formatDate(data.data.start_date ));
                    $("#task-modal-edit input[name='end_date']").val(formatDate(data.data.end_date ));

                    $("#task-modal-edit .list-user :checkbox").each(function (e) {
                        if(e <= data.data_user.length-1){
                            $(this).val() == data.data_user[e].id ? $(this).prop("checked", true) : ''
                        }
                    });

                    $('#Status option').each(function() {
                        if($(this).val() == data.data.status) {
                            $(this).prop("selected", true);
                        }
                    });

                    $('#priority option').each(function() {
                        if($(this).val() == data.data.priority) {
                            $(this).prop("selected", true);
                        }
                    });

                    $('#audio-progress-bar').text(parseInt(data.data.progress*100) +"%")
                    $('#audio-progress-bar').css('width',data.data.progress*100 +"%")
                    $('#draggable-point').draggable({
                        axis: data.data.progress*100 +"%",
                        containment: "#audio-progress",
                        top:0
                    });
                    $('#draggable-point').css({
                        'left': data.data.progress*100 +"%",
                    });
                    $('#progress').val(data.data.progress);
                    if(data.data.progress==1){
                        $('.audio-progress .bar').addClass('done')
                    }
                },
            });
        }

        function clearSeession() {
            $('.text-danger').hide()
        }

        function formatDate(date) {
            return  new Date(date).toISOString().substring(0, 10);
        }
    </script>

@endpush
