@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                        <div class="d-flex bd-highlight">
                            <div class="p-2 flex-grow-1 bd-highlight">
                                <h3>Your Project</h3>
                            </div>
                            <div class="p-2 bd-highlight"></div>
                        </div>
                    <div class="row">
                        @foreach($project as $row)
                            <div class="col-md-3 col-sm-6">
                                    <a href="{{route('agile.task',$row['id'])}}" >
                                        <div class="agile-item">
                                            <div class="d-flex bd-highlight">
                                                <div class="p-2 flex-grow-1 bd-highlight">
                                                    <div class="item-name">
                                                        {{$row['text']}}
                                                    </div>
                                                </div>
                                                <div class="p-2 bd-highlight">{{$row['duration']}}day ({{$row['duration']*8}}h)</div>
                                            </div>
                                            <div class="item-title">
                                                {{$row['text']}}
                                            </div>
                                            <div class="item-auth">
                                                Auth: {{$row['auth_name']}}
                                            </div>

                                        </div>
                                    </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
    <style>
        .agile-item{
            background: #1de9b6;
            margin-bottom: 20px;
            padding: 7px;
            border-radius: 5px;
            cursor: pointer;
        }
        .agile-item .item-title, .item-name{
            font-weight: bold;
        }
        a{
            color: black;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
@endsection
@push('script')
    <script>

    </script>
@endpush
