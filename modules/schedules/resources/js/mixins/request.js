const axios = require("axios");


export const requestsMixin = {
    methods: {
        fetchTaskProject(data) {
            axios.get(`/projects/api/project-task/` + data)
                .then(response => {
                    this.$store.commit("setEvents", response.data);
                })
        },
    }
};
