import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        tasks: [],
        messages: [],
        message: '',
        currentPage:1,
    },
    mutations: {
        setEvents(state, payload) {
            state.tasks = payload;
        },
        setMessages(state, payload) {
            state.messages = payload;
        }
    },
    actions: {}
});
